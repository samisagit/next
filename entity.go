package next

import (
	"sort"
	"strings"
)

type actionList []string

func (al actionList) encode(e encoder) string {
	if len(al) == 0 {
		return "defaultSet"
	}
	sort.Strings(al)
	joinedActions := strings.Join(al, ",")

	return e.encode(joinedActions)
}

func (neededActionList actionList) satisfied(givenActionList actionList) bool {
	if len(neededActionList) < 1 {
		return true
	}
	result := true
	for _, neededVal := range neededActionList {
		currentNeededValSatisfied := false
		for _, givenVal := range givenActionList {
			if givenVal == neededVal {
				currentNeededValSatisfied = true
				break
			}
		}
		if !currentNeededValSatisfied {
			return false
		}
	}

	return result
}

