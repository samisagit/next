package next

import (
	"testing"
	b64 "encoding/base64"
)

type testEncoderDecoder struct{}

func (testEncoderDecoder) encode(data string) string {
	return b64.StdEncoding.EncodeToString([]byte(data))
}

func (testEncoderDecoder) decode(data string) string {
	sDec, _ := b64.StdEncoding.DecodeString(data)
	return string(sDec)
}

var ed = testEncoderDecoder{}

var actions = actionList{
	"confirmEmail",
	"addAvatar",
	"addFriends",
}

var actionMap = map[int][]string{
	0: actionList{
		actions[0],
	},
	1: actionList{
		actions[1],
		actions[2],
	},
}

var hash = actionList(actionMap[0]).encode(ed)


func TestNewNextGenerator(t *testing.T) {
	ap := NewNextGenerator(actionMap, ed)

	al, err := ap.Provide(hash)
	if err != nil {
		t.Error(err)
	}

	itemsNeeded := map[string]bool{
		actions[1]: false,
		actions[2]: false,
	}
	for _, a := range al {
		itemsNeeded[a] = true
	}
	for key, val := range itemsNeeded {
		if !val {
			t.Errorf("item %s was not returned", key)
		}
	}
}

