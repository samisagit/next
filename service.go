package next

import (
	"fmt"
)

type encoder interface {
	encode(string) string
}

type decoder interface {
	decode(string) string
}

type encoderDecoder interface {
	encoder
	decoder
}

type ActionProvider struct {
	decisionTree map[string]actionList
	ed encoderDecoder
}

func (ap ActionProvider) Provide(hash string) (actionList, error) {
	action, ok := ap.decisionTree[hash]
	if !ok {
		return actionList{}, fmt.Errorf("unexpected failure - no key at hash %s", hash)
	}

	return action, nil
}

func NewNextGenerator(
	actions map[int][]string,
	ed encoderDecoder,
) ActionProvider {
	actionMap := map[int]actionList{}

	// get all the possible actions
	uniqueActionsMap := map[string]bool{}
	uniqueActionsSlice := actionList{}
	for key, actionItems := range actions {
		for _, action := range actionItems {
			uniqueActionsMap[action] = true
		}
		actionMap[key] = actionList(actionItems)
	}
	// construct slice of unique values
	for action, _ := range uniqueActionsMap {
		uniqueActionsSlice = append(uniqueActionsSlice, action)
	}

	decisionTree :=  buildActionSwitch(uniqueActionsSlice, actionMap, ed)

	return ActionProvider{
		decisionTree: decisionTree,
		ed: ed,
	}
}

func buildActionSwitch(
	uniqueActions actionList,
	actionMap map[int]actionList,
	ed encoderDecoder,
) map[string]actionList {
	possibleActionSets := actionPermutations(uniqueActions)
	switchData := map[string]actionList{}

	for _, set := range possibleActionSets {
		completedActionIndex := 0
		encodedKey := set.encode(ed)

		// check which index levels are already complete
		for _, val := range actionMap {
			satisfiesLevel := val.satisfied(set)
			if !satisfiesLevel {
				break
			}
			completedActionIndex++
		}

		// quit early if everything is done
		if completedActionIndex >= (len(actionMap) - 1) {
			switchData[encodedKey] = actionList{}
		}

		// need to create an idex for empty actionset
		switchData["defaultSet"] = actionMap[0]

		// check items in actionMap index directly above completedActionIndex that are not present
		neededItems := actionMap[completedActionIndex]
		missingItems := make([]string, len(neededItems))
		copy(missingItems, neededItems)
		for _, neededItem := range neededItems {
			for _, givenVal := range set {
				if neededItem == givenVal {
					for p, v := range missingItems {
						if v == neededItem {
							missingItems = append(missingItems[:p], missingItems[p+1:]...)
							break
						}
					}
				}
			}
		}
		switchData[encodedKey] = missingItems
	}

	return switchData
}

func actionPermutations(set actionList) []actionList {
	length := uint(len(set))
	subsets := []actionList{}

	for subsetBits := 1; subsetBits < (1 << length); subsetBits++ {
		var subset actionList

		for i := uint(0); i < length; i++ {
			if (subsetBits>>i)&1 == 1 {
				subset = append(subset, set[i])
			}
		}
		subsets = append(subsets, subset)
	}
	return subsets
}
